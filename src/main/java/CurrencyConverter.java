import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CurrencyConverter {

	public static final String EXCHANGE_RATES_API_BASE = "https://api.exchangeratesapi.io/latest?base=";

	public static final String ERR_INCORRECT_ARGS = "ERROR: Incorrect arguments. Please input <amount> <from_currency> <to_currency>. eg. 12 EUR USD";
	public static final String ERR_INCORRECT_AMOUNT = "ERROR: Amount is inappropriate.";
	public static final String ERR_NOT_FOUND_CURRENCY = "ERROR: Inputted <to_currency> is not found.";
	public static final String ERR_INVALID_CURRENCY = "ERROR: Inputted <from_currency> is not valid.";
	public static final String ERR_CONNECTION = "ERROR: There is a problem with the API. Contact the system administrator.";

	public static final String INFO_THANKS = "Thanks for using this currency converter.";

	public static void main(String[] args) {

		// Check if there is correct number of arguments set
		if (args.length != 3) {
			System.out.println(ERR_INCORRECT_ARGS);
		} else {
			// Get input amount to be converted
			String amountStr = args[0];
			// Get base currency string
			String fromCurrency = args[1];
			// Get to currency string
			String toCurrency = args[2];
			// Convert inputted amount from inputted base currency to target currency.
			try {
				convert(fromCurrency, toCurrency, amountStr);
			} catch (IOException e) {
				System.out.println(ERR_CONNECTION);
			}
		}
		System.out.println(INFO_THANKS);
	}

	/**
	 * Main conversion function
	 * @param fromCurrency
	 * @param toCurrency
	 * @param amountStr
	 * @throws IOException
	 */
	public static void convert(String fromCurrency, String toCurrency, String amountStr) throws IOException {
		// Check if inputted amount is numeric or not
		Double inputAmount = isAmountValid(amountStr);
		if (inputAmount == null) {
			System.out.println(ERR_INCORRECT_AMOUNT);
			return;
		}
		// Call API and get the rates info for the set base(from) currency
		String ratesInfoStr = getCurrencyRates(fromCurrency);

		// If base currency is not supported or there is error returned from API
		if (ratesInfoStr == null) {
			System.out.println(ERR_INVALID_CURRENCY);
		} else {
			JSONObject ratesJsonObj = new JSONObject(ratesInfoStr);
			// Check if rates info are successfully retrieved
			if (ratesJsonObj.has("rates")) {
				Map<String, Double> ratesMap = getRates(ratesJsonObj);
				if (ratesMap.containsKey(toCurrency)) {
					// Display converted amount
					System.out.println(getConversionAmount(ratesMap, toCurrency, inputAmount, fromCurrency));
				} else {
					// If currency rate is not found or not supported.
					System.out.println(ERR_NOT_FOUND_CURRENCY);
				}
			} else if (ratesJsonObj.has("error")) {
				// If base currency is not supported or there is error returned from API
				System.out.println(ERR_INVALID_CURRENCY);
			}
		}
	}

	/**
	 * Get the info list of currency rates supported by the API
	 * @param ratesJsonObj
	 * @return Map<String, Double> object containing the rates info
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 */
	public static Map<String, Double> getRates(JSONObject ratesJsonObj)
			throws JsonMappingException, JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		// Get the rates JSON attribute
		String ratesStr = ratesJsonObj.get("rates").toString();
		TypeReference<HashMap<String, Double>> typeRef = new TypeReference<HashMap<String, Double>>() {};
		// Map JSON array string to a HashMap<String, Double> object and return
		return objectMapper.readValue(ratesStr, typeRef);
	}

	/**
	 * Compute for the conversion and return the formatted output string
	 * @param ratesMap
	 * @param toCurrency
	 * @param inputAmount
	 * @param fromCurrency
	 * @return Formatted output string eg.12 EUR = 13.36 USD
	 */
	public static String getConversionAmount(Map<String, Double> ratesMap, String toCurrency, Double inputAmount,
			String fromCurrency) {
		// Calculate conversion amount
		Double convertedAmount = ratesMap.get(toCurrency).doubleValue() * inputAmount;
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%.2f", inputAmount));
		sb.append(" ");
		sb.append(fromCurrency);
		sb.append(" = ");
		sb.append(String.format("%.2f", convertedAmount));
		sb.append(" ");
		sb.append(toCurrency);
		// Return formatted string
		return sb.toString();
	}

	/**
	 * Check if inputted amount is numeric(parseable to Double) or not
	 * @param strNum
	 * @return TRUE if valid amount
	 */
	public static Double isAmountValid(String strNum) {
		if (strNum == null) {
			return null;
		}
		try {
			return Double.parseDouble(strNum);
		} catch (NumberFormatException nfe) {
			return null;
		}
	}

	/**
	 * Fetch the currency rates info in JSON form
	 * @param base
	 * @return JSON string containing the currency rates info available in the API
	 * @throws IOException
	 */
	public static String getCurrencyRates(String base) throws IOException {
		// Connect with the API
		HttpURLConnection connection = (HttpURLConnection) new URL(EXCHANGE_RATES_API_BASE + base)
				.openConnection();
		connection.setRequestMethod("GET");

		int responseCode = connection.getResponseCode();
		if (responseCode == 200) {
			Scanner scanner = new Scanner(connection.getInputStream());
			StringBuilder responseSb = new StringBuilder();
			while (scanner.hasNextLine()) {
				responseSb.append(scanner.nextLine());
				responseSb.append("\n");
			}
			scanner.close();
			return responseSb.toString();
		}

		// An error happened
		return null;
	}
}
